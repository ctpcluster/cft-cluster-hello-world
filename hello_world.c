/* Source http://www.slac.stanford.edu/comp/unix/farm/mpi.html */
#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
  int numprocs, rank, namelen;
  char processor_name[MPI_MAX_PROCESSOR_NAME];

  MPI_Init(&argc, &argv); 	/* starts MPI */
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);	/* get number of processes */
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); /* get current process id */
  MPI_Get_processor_name(processor_name, &namelen); /* get processor name - hostname*/

  printf("Process %d on %s out of %d\n", rank, processor_name, numprocs);

  MPI_Finalize();
}
